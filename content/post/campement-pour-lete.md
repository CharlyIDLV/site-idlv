+++
author = "quentin.orhant@gmail.com"
date = 2017-08-01T14:10:18Z
image = "/images/2017/08/couv.jpg"
description = ""
draft = false
slug = "campement-pour-lete"
title = "Un campement pour l'été"

+++

![](/images/2017/08/1.jpg)<br>

Suite à l'appel à projet Odyssée Urbaine  2017, dans le contexte et dans la continuité du projet de concertation pour l’aménagement du quartier Armorique, engagé par la ville de Rennes, nous imaginons avec Adélaïde Fiche (fondatrice de « Folk paysages ») un chantier ouvert aux habitants.

Nous proposons de les accompagner dans l'élaboration d'un campement éphémère en bordure du canal d'Ille-et-Rance. Avec pour objectif de favoriser la convivialité et la rencontre, ces temps partagés seront l’occasion d’impliquer petits et grands dans la réalisation d’un mobilier temporaire, simple, créatif, ludique, pas cher, sans risque… tout en valorisant l’intelligence collective dans un cadre informel. 

Récupérant des matériaux, partageant les compétences, les savoir-faire et les idées de chacun, ces temps de fabrication et de création collectifs sont aussi l’occasion de questionner la façon dont on aménage la ville et la manière dont chacun s’approprie son environnement.
<br>
<center><img src="/images/2017/08/19221650_1630621040281404_4141844150276593242_o.jpg" alt="Mountain View" style="width:50%;height:50%"></center><br><br>

{{< mark "Premiers pas :" >}}

{{< pswp-init >}}

{{< gallery >}}

{{< figure link="/images/2017/08/2.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/3.jpg" thumb="-thumb" size="1200x600" >}}

{{< /gallery >}}



{{< mark "Atelier fabrication de drapeaux :" >}}

{{< gallery class="col3" >}}

{{< figure link="/images/2017/08/14.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/15.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/16.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/17.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/18.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/19.jpg" thumb="-thumb" size="1200x600" >}}

{{< /gallery >}}


{{< mark "Réalisation d'une calade :" >}}

{{< gallery >}}

{{< figure link="/images/2017/08/30.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/31.jpg" thumb="-thumb" size="1200x600" >}}

{{< /gallery >}}

<br>

{{< mark "Installation des drapeaux  :" >}}


{{< gallery >}}

{{< figure link="/images/2017/08/21.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/22.jpg" thumb="-thumb" size="1200x600" >}}

{{< /gallery >}}


![](/images/2017/08/23.jpg)<br>

{{< mark "Fabrication du mobilier  :" >}}


{{< gallery >}}

{{< figure link="/images/2017/08/24.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/26.jpg" thumb="-thumb" size="1200x600" >}}

{{< /gallery >}}

<br>

![](/images/2017/08/_Plan---Transat-et-chaise-Zac-1.jpg)

<br><br>

![](/images/2017/08/41.jpg)



{{< gallery >}}

{{< figure link="/images/2017/08/4.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/5.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/6.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/7.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/9.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/11.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/12.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/25.jpg" thumb="-thumb" size="1200x599" >}}
{{< figure link="/images/2017/08/27.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/29.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/13.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/34.jpg" thumb="-thumb" size="1200x599" >}}
{{< figure link="/images/2017/08/35.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/36.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/39.jpg" thumb="-thumb" size="1200x600" >}}
{{< figure link="/images/2017/08/40.jpg" thumb="-thumb" size="1200x600" >}}

{{< /gallery >}}


<br>

{{< mark "Sources :" >}}

* <a href="https://www.dropbox.com/s/tti26585jo83o3y/_Plan%20-%20Transat%20et%20chaise%20Zac.pdf?dl=0" target="_blank">Plans du mobilier en palettes (pdf)</a>
* <a href="https://www.folk-paysages.fr/" target="_blank">Folk Paysages</a>

<br>