+++
author = "Charly2"
date = 2015-03-15T13:32:00Z
image = "/images/2017/05/tipibot2-1.jpg"
description = ""
draft = false
slug = "tipibot"
title = "Tipibot"

+++

<center>
    <iframe src="https://player.vimeo.com/video/126355254" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    <br>
    <a href="https://vimeo.com/126355254" target="newtab">Le Tipibot</a>
</center>

<br>

Le Tipibot est le fruit d’un développement collaboratif, conçu avec la volonté de créer un objet sous licence libre, il a entre autre permis au collectif IDLV de promouvoir au fil des différents projets, les principes de l’open source.
La partie logicielle et certaines pièces sont déjà le fruit du travail personnel de plusieurs makers répartis à travers le monde.

Depuis 2015, le collectif, utilise et diffuse cet outil pouvant être à la fois un instrument de création à destination des makers, des artistes, des professionnels, ou encore un support pédagogique pour un premier pas dans le vaste monde de la fabrication numérique.

Très attachés au rendu pictural de ses propositions artistiques, le collectif IDLV travail et ne cesse de multiplier les usages pour transposer et adapter ce robot qui dessine et qui graff à différents contextes. Parfait trait d’union entre le dessin et la photographie, permettant de dessiner de très grands formats, pouvant même concurrencer les panneaux d’affichage publicitaire, il est devenu depuis le compagnon idéal pour la réalisation d’oeuvres s’affichant dans l’espace public.

<br>

{{< mark "Sources :" >}}

* <a href="https://www.instructables.com/id/TIPIBOT-Le-Robot-Qui-Dessine/" target="_blank">La notice d'assemblage sur instructables.com</a>
* <a href="http://www.thingiverse.com/thing:2304175" target="_blank">Les fichiers 3D sur thingiverse.com</a>
* <a href="https://github.com/euphy/polargraph_server_polarshield" target="_blank">Le firmware Polargraph par Euphy sur github</a>
* <a href="https://github.com/RickMcConney/PenPlotter" target="_blank">Le firmware PenPlotter par Rick McConney sur github.com</a>


