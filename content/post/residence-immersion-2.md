+++
author = "Charly2"
date = 2015-12-01T22:11:00Z
image = "/images/2017/05/Sans-titre-2.jpg"
description = ""
draft = false
slug = "residence-immersion-2"
title = "Résidence Immersion"

+++

Le 20 novembre 2015, nous sommes invités en résidence à l'ancienne Faculté des Sciences de Rennes pour un mois de création et d'appropriation de l'espace.

Dans le cadre du projet « IMMERSION », l’association l’Artillerie propose après trois semaines de résidence et d’installation, une exposition déambulatoire, cheminant à travers l’univers créatif de chaque intervenant. Au total, une quinzaine d’artistes, d’artisans et de collectifs sont invités à participer à ce projet ; aménageant, sculptant, modifiant leur espace en toute liberté, avec la volonté de valoriser le lieu et de faire découvrir au public leurs travaux et réflexions.

Nous avons donc déménagé dans une des salles du deuxième étage de l’établissement, et créé une ambiance pleine de cartographies, de robots qui dessinent, d’imprimantes 3d, d’ordinateurs et de gigantesques immeubles miniatures.

Longeant un tipi, lieu de discussion et de convivialité, une rivière peinte à la craie se fait le témoin des déplacements des visiteurs. Un robot dessine pendant toute la durée de la résidence une carte de Rennes très grand format. Pendant qu’un autre, esquisse à sa manière les portraits des six scientifiques émérites dont les médaillons ornent la façade sud du bâtiment.

Nous avons par ailleurs organisé deux ateliers participatifs interrogeant les visiteurs sur le lieu, son rayonnement dans la ville, et ça place dans l’imaginaire collectif.

<br>

![](/images/2017/05/tumblr_o0ypifHmRT1s1wy0go1_540.jpg)

<br>

{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion1.jpg" thumb="-thumb" size="947x532" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion2.jpg" thumb="-thumb" size="944x532" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion3.jpg" thumb="-thumb" size="942x530" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion4.jpg" thumb="-thumb" size="949x534" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion5.jpg" thumb="-thumb" size="947x534" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion7.jpg" thumb="-thumb" size="947x534" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion9.jpg" thumb="-thumb" size="949x535" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion10.jpg" thumb="-thumb" size="948x533" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion11.jpg" thumb="-thumb" size="947x534" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion12.jpg" thumb="-thumb" size="948x533" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion13-1.jpg" thumb="-thumb" size="945x533" >}}
{{< figure link="/images/2017/05/07_photos-r-sidence-immersion14.jpg" thumb="-thumb" size="948x532" >}}

{{< /gallery >}}
