+++
author = "quentin.orhant@gmail.com"
date = 2016-07-07T22:44:00Z
image = "/images/2017/05/aegegrzeg-2.jpg"
description = ""
draft = false
slug = "grande-synthe"
title = "Grande-Synthe"

+++

<br>
<center>
    
<iframe src="https://player.vimeo.com/video/216487260" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
 
<a href="https://vimeo.com/216487260">Grande-Synthe - 2017</a>

</center>

<center>Musique : The Souljazz Orchestra - Mista President </center>

<br>


En juillet 2016, à cinq cent mètres de l'ancien camps humanitaire de Grande-Synthe, nous avons fait graffer notre robot sans raccordement électrique. 

A l'occasion de cette expérimentation, nous avons réalisé une fresque représentant les zones de conflits et les principaux flux migratoire.

{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/05/2016-07-22-22-49-36-1.jpg" thumb="-thumb" size="1200x794" >}}
{{< figure link="/images/2017/05/2016-07-22-22-50-32-1.jpg" thumb="-thumb" size="1200x794" >}}
{{< figure link="/images/2017/05/2016-07-22-23-19-34-1.jpg" thumb="-thumb" size="1200x794" >}}

{{< /gallery >}}