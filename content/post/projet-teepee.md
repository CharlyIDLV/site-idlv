+++
author = "quentin.orhant@gmail.com"
date = 2016-05-02T00:42:00Z
image = "/images/2017/05/46-1-thumb.jpg"
description = ""
draft = false
slug = "projet-teepee"
title = "Projet Teepee"

+++

{{< mark "Mars 2016 :" >}}


{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/05/12828955_1163754790301367_821865213506219939_o.jpg" thumb="-thumb" size="2048x1356" >}}
{{< figure link="/images/2016/10/10.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/21.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/22.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/38.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/41.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/46.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/47.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/49.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/50.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/10/55.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/58.jpg" thumb="-thumb" size="999x662" >}}

{{< /gallery >}}


Vendredi 4 mars 2016 dans la soirée, nous avons pris la route pour nous rendre à Calais. Le but de ce voyage, aller à la rencontre des réfugiés et des associations sur place, afin de se présenter et d'envisager si possible de mettre en place des moments d'animation et de création avec les enfants.

En arrivant nous avons été accueillis et orientés par les bénévoles et les coordinateurs de l'association Utopia 56. Grâce eux, nous avons pu notamment apporter notre contribution et aider à la préparation du futur camp humanitaire de Grande-Synthe. Ils nous ont également logés gratuitement la nuit du samedi.

Le lendemain, après un passage sur le futur camp de Médecins Sans Frontières, nous avons pris la route pour nous rendre sur les deux camps de la région. Nous avons chercher à prendre contact avec les associations travaillant autour de l'éducation. L'occasion aussi de créer un premier contact avec tous ces gens ayant été obligés de quitter leur pays pour traverser l'Europe en quête d'un avenir plus serein.

Après deux jours intenses, remplis d'humanité, nous avons pu créer quelques contacts et c'est très déterminés que nous prévoyons d'y retourner.
<br><br>


{{< mark "Avril 2016 :" >}}

{{< gallery >}}
{{< figure link="/images/2016/10/24-1.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/10/25-1.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/7-1.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/10/29-1.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/31-1.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/10/32-1.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/33-1.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/34-1.jpg" thumb="-thumb" size="999x662" >}}
{{< figure link="/images/2016/10/36-1.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/10/42-1.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/10/44-1.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2016/10/46-1.jpg" thumb="-thumb" size="1000x662" >}}

{{< /gallery >}}


Depuis, une collecte de matériel pédagogique a été mise en place et le 12 avril dernier, onze animateurs, éducateurs et artistes partent de Bretagne pour venir créer, de manière ponctuelle et spontanée, des ateliers d’animation et d’expression artistique.

Installant nos ateliers dans différents endroits du camp, nous avons pu montrer l’importance pour les enfants de ces temps destinés à l’expression individuelle sous différentes formes. Allant à la rencontre des différents acteurs présents sur place (associations locales, coordinateurs, bénévoles, intervenants et habitants du camp). Nous avons fait part de notre initiative en présentant le dossier de notre projet, invitant chacun à le lire et à se l’approprier. 

Pendant cinq jours, nous avons été au meilleur endroit pour penser ensemble la suite : nous souhaiterions imaginer un espace de convivialité, un cadre chaleureux, équipé de jouets et de matériel pédagogique, un lieu où les parents, les éducateurs et les bénévoles pourraient aisément animer des ateliers destinés aux enfants.

Nous pourrions penser et construire ensemble, réfugiés et bénévoles, un lieu à part entière où stocker du matériel et disposer ainsi d’un endroit identifiable dédié aux animations avec les enfants.

Aujourd’hui, le projet est validé par Damien Carême, le Maire de la commune de Grande-Synthe. Le temps de préciser le projet, de trouver des financements et des partenaires, nous envisageons de venir aider à la construction de ce lieu au début du mois de juillet. 
<br><center><div data-configid="8103426/44278231" style="width:800px; height:600px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script></center><br>

{{< mark "Juillet 2016 :" >}}

{{< gallery >}}
{{< figure link="/images/2017/05/2016-07-15-20-39-42.jpg" thumb="-thumb" size="1200x794" >}}
{{< figure link="/images/2017/05/2016-07-20-13-31-02.jpg" thumb="-thumb" size="1200x794" >}}
{{< figure link="/images/2017/05/2016-07-20-13-44-16.jpg" thumb="-thumb" size="1200x794" >}}
{{< figure link="/images/2017/05/2016-07-20-13-56-06.jpg" thumb="-thumb" size="1200x794" >}}
{{< figure link="/images/2017/05/2016-07-20-17-18-02.jpg" thumb="-thumb" size="1200x794" >}}
{{< figure link="/images/2017/05/2016-07-21-11-05-20.jpg" thumb="-thumb" size="1200x794" >}}
{{< figure link="/images/2017/05/2016-07-21-11-41-08.jpg" thumb="-thumb" size="1200x794" >}}
{{< figure link="/images/2017/05/2016-07-22-15-41-44.jpg" thumb="-thumb" size="1200x794" >}}

{{< /gallery >}}



Juillet 2016, nous reprenons la route de Calais avec les mêmes motivations.

Nous passerons la semaine dans le jardin d’enfant du camp de Grande-Synthe aux côtés de Milie, une jeune bénévole anglaise accompagnée par d’autre bénévoles, qui tant bien que mal apportent chaque jour distraction et bienveillance à la quarantaine d’enfants qui viennent leur rendre visite.

Situé au milieu du camp de la Linière, ce baraquement coloré, où l’on laisse ses chaussures à l’entrée, remplie de cahiers, de stylo, de mots d’enfants, de quoi dessiner et de quoi jouer, apaise le camp et les tensions qui s’accumulent. 
Sensé fonctionner comme un espace permettant les activités « périscolaire », nous occuperons et partagerons ce petit espace récréatif avec eux durant la semaine. Pendant que d’autre bénévoles transmettent des cours de français et d’anglais aux enfants plus âgés, d’autre plus jeune courent, rient, et se chamaillent… Nous arbitrerons des petits matches de box, construirons les plus beaux châteaux en lego, fabriquerons des marionnettes et dessinerons des fresques multicolores

Très heureux du bonheur même fugace que nous avons partagé avec ces enfants, nous quitterons Grande-Synthe un pincement dans le cœur. 
Entre l’intérêt et les égaux de chacun parfois inadaptés, entre l’affaiblissement des solidarités extérieurs de plus en plus effarant, l'inertie des pouvoir public, les traumatismes trop évidents qui dévastent ces enfants et ces gens privés de liberté, nous restons quelque peu impuissants.