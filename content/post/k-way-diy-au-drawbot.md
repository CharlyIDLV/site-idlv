+++
author = "quentin.orhant@gmail.com"
date = 2016-09-02T00:41:00Z
image = "/images/2017/02/14469236_10154540736097630_436305070_n-1.jpg"
description = ""
draft = false
slug = "k-way-diy-au-drawbot"
title = "Vêtement libre"

+++

En collaboration avec la créatrice Anna Le Reun (Do It Together !), IDLV imagine un vêtement personnalisable grâce aux outils de fabrication numérique.

Après un phase d’esquisse et de patronage classique, les éléments du vêtement sont numérisés pour créer un fichier numérique du patron.

Ce parton dématérialisé peut être reporté sur n’importe quel tissu, transformé, mis à une autre échelle, à l’aide d’un ordinateur, d’un vidéoprojecteur ou d’un traceur mural …

Des essais de motifs et paternes ont été testés en atelier sur du Tyvek, un matériau synthétique non-tissé fabriqué à partir de fibres de polyéthylène. Un premier Kway a déjà été cousu, il est superbe ! Découvrez les premiers résultats de nos expérimentations. Et vous pouvez aussi télécharger le patron, sous licence Creative Commons BY-NC-SA, pour faire vous-même votre imperméable !
<br>
![](/images/2017/05/patron-kway-tyvek.jpg)
<br><br>

{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/05/DSC4881.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC4895.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC5125.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC5124.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/02/zfagf.jpg" thumb="-thumb" size="960x636" >}}
{{< figure link="/images/2017/02/bloc-elastiq.jpg" thumb="-thumb" size="1002x663" >}}

{{< /gallery >}}


<br>{{< mark "Source :" >}}

* <a href="https://www.dropbox.com/s/z8mbwfwx9sd3syt/patronnage-copie.pdf?dl=0" target="_blank">Fichier pdf de la veste</a> 

