---
author: "Gutierrez Charly"
date: 2018-04-30T10:51:02Z
image: "/images/2018/04/Acceuil-.jpg"
description: ""
draft: false
slug: "au-fil-du-granite"
title: "Au fil du granite"

---

![Image-d-intro-5](/images/2018/04/Image-d-intro-5.jpg)


{{< mark "La démarche :" >}}

Par la fabrication de microscopes open source réalisés à partir de lentilles d’appareils photo jetables et d’un peu d’impression 3D, le collectif IDLV propose aux élèves de l’école publique Mario Ramos de Lanhélin d’observer des échantillons de « granite bleu » grossis 100 fois.

Par un va et vient entre ce qu’ils observent et ce qu’ils représentent, le dessin d’observation permet aux élèves d’appréhender le granite à travers une approche à la fois sensible et complexe du réel, leur permettant d’affiner leurs interprétations et de mieux comprendre que chaque matériau possède des propriétés particulières, qui sont utilisées dans des contextes bien précis.


{{< pswp-init >}}

{{< gallery >}}

{{< figure link="/images/2018/04/0-.jpg" thumb="-thumb" size="1500x1126" >}}
{{< figure link="/images/2018/04/2.jpg" thumb="-thumb" size="1500x1125" >}}
{{< figure link="/images/2018/04/3.jpg" thumb="-thumb" size="1500x1125" >}}
{{< figure link="/images/2018/04/5.jpg" thumb="-thumb" size="1500x1125" >}}
{{< figure link="/images/2018/04/6.jpg" thumb="-thumb" size="1500x1125" >}}
{{< figure link="/images/2018/04/7.jpg" thumb="-thumb" size="1500x1125" >}}
{{< figure link="/images/2018/04/8-2.jpg" thumb="-thumb" size="1500x1125" >}}
{{< figure link="/images/2018/04/9-1.jpg" thumb="-thumb" size="1500x1125" >}}

{{< /gallery >}}


{{< mark "Sources :" >}}

* [Lien vers les fichiers 3D du microscope imprimable](https://www.thingiverse.com/thing:77450)
