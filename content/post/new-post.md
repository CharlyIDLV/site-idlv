+++
author = "quentin.orhant@gmail.com"
date = 2016-09-02T00:41:00Z
image = "/images/2017/02/14469236_10154540736097630_436305070_n-1.jpg"
description = ""
draft = false
slug = "k-way-diy-au-drawbot"
title = "Vêtement libre"

+++

This is a great new post!

<br><br>

{{< pswp-init >}}
{{< gallery >}}
{{< figure link="/images/2017/05/DSC4881.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC4895.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC5125.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/05/DSC5124.jpg" thumb="-thumb" size="1000x662" >}}
{{< figure link="/images/2017/02/zfagf.jpg" thumb="-thumb" size="960x636" >}}
{{< figure link="/images/2017/02/bloc-elastiq.jpg" thumb="-thumb" size="1002x663" >}}

{{< /gallery >}}


<br>{{< mark "Source :" >}}

* <a href="https://www.dropbox.com/s/z8mbwfwx9sd3syt/patronnage-copie.pdf?dl=0" target="_blank">Fichier pdf de la veste</a> 

