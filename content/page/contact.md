---
date: 2017-12-01T00:00:35Z
title: Contact
menu: main
weight: -200
---

<center>
<br>

**Atelier IDLV - Indiens Dans La Ville**

2 rue du prés du bois

Rennes - 35000

FRANCE

<br>


**Association IDLV - Indiens Dans La Ville**

6 rue du clos simon

Rennes - 35000

FRANCE

<br>

**Artistes du collectif :**

Corentin Le Bris

Arthur Masson

Quentin Orhant

Charly Gutierrez

<br>

<a class="transparent" href="mailto:idlv.contact@gmail.com"><img src="/images/2017/05/mail-1.png"></a>

<a href="mailto:idlv.contact@gmail.com">idlv.contact@gmail.com</a>

<br>

<a class="transparent" href="http://twitter.com/collectif_idlv" target="newtab"><img src="/images/2017/05/Sans-titre---3-02-1.png"></a>

twitter.com/collectif_idlv

<br>

<a class="transparent" href=" https://www.facebook.com/idlv.co/" target="newtab"><img src="/images/2017/05/Sans-titre---3-03.png"></a>

facebook.com/idlv.co/

<br>

