+++
author = "Arthur Masson"
date = 2012-09-27T07:37:00Z
description = ""
draft = false
slug = "propos"
title = "Propos"

+++

"Indiens Dans La Ville" est un collectif d'artistes pluridisciplinaires.

Depuis 2013, IDLV souligne et interroge les contrastes qui peuvent surgir entre la Nature et la Ville, la Ville et ses Habitants. Comment notre environnement conditionne nos modes de vie ? 

Artistes et makers dans l'âme, les membres du collectif associent leurs connaissances à travers différents procédés : travaux photographiques, fanzines, affichage urbain, impression 3D, robotique, logiciels, installations dans l’espace public … Poursuivant une réflexion sur les manières de voir la ville et les différentes façons de la représenter.

Passionnés par les nouvelles technologies, à l'heure où la fabrication numérique et les projets opensource se démocratisent, nous défendons les valeurs avant-gardistes portées par le mouvement maker. Le collectif trouve ainsi dans le partage et l’échange, les moyens d’imaginer des projets toujours plus pertinents; par le décloisonnement de pratiques aussi variées que la peinture, l’architecture, les marionnettes ou encore la couture.

Convaincus par l’importance de la créativité et de l’inventivité dans notre société, souhaitant ouvrir et diffuser notre réflexion à un plus large public, en 2016 le collectif devient l’association IDLV - Indiens Dans La Ville. Aujourd'hui, IDLV se veut acteur du changement visant à favoriser une manière logique et humaniste de vivre en cohérence avec l’environnement, la nature et les hommes dans le contexte urbain d'aujourd’hui. 

<br>
<center>
    
![logo-carr--1](/content/images/2018/01/logo-carr--1.jpg)
</center>
<br>
