+++
author = "quentin.orhant@gmail.com"
date = 2016-12-20T14:16:40Z
description = ""
draft = false
slug = "association"
title = "Association"

+++


Depuis juillet 2016, le collectif d'artistes est structuré en association loi 1901 reconnue d’intérêt général.


L'association a pour objet :

* Favoriser l’accès à l’art et la culture au plus large public possible.
* Imaginer la ville et le monde de demain, avec une conscience éthique, sociétale et environnementale.
* Amener un rapport à la technique engagé, inventif et artistique.
* Encourager l'exploration des intersections entre domaines traditionnellement séparés.


Le Bureau :

* Président : Brieuc MORVAN
* Secrétaire : Aurélie CHEVALIER
* Trésorier : Kévin LECOZ


Soutenir l'association IDLV : <a href="/adherez">==**ADHÉRER**==</a> ou <a href="/donnez">==**FAIRE UN DON**==</a>

* L'adhésion vous permet de soutenir humainement et financièrement le projet du collectif d'artistes.
* Le don et l'adhésion ouvrent droit à une réduction fiscale de 66% car ils remplissent les conditions générales du code général des impôts.
* Vous êtes évidemment bienvenu si vous souhaitez participer activement en devenant bénévole !

Soutiens et partenaires :

<a href="http://www.hotelpasteur.fr/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/pasteur-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://labelledechette.com/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/la-belle-dechette-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://myhumankit.org/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/mhk-1.png" ></a>&nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/LabBroPondi/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/LAB-BRO-PONDI-2.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://www.labfab.fr/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/labfab-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/Makemefamily/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/makeme-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://keureskemm.fr/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/keureskemm-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://www.electroni-k.org/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/electronik-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.ateliersmedicis.fr" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/ateliers-m-dicis-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="http://www.pingbase.net/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/ping-1.png" ></a>&nbsp;&nbsp;&nbsp;<a href="https://lesautrespossibles.fr/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/06/lesautrespossibles-1.png"></a>&nbsp;&nbsp;&nbsp;<a href="https://www.folk-paysages.fr/" target="newtab" style="display: inline-block;"><img src="/content/images/2017/08/folk.jpg"></a>
<br>

