+++
date = 2018-09-26T21:32:00Z
description = ""
draft = false
slug = "contacts"
title = "Contacts"

+++


<center>
<br>

**Atelier IDLV - Indiens Dans La Ville**
2 rue du prés du bois
Rennes - 35000
FRANCE
<br>


**Association IDLV - Indiens Dans La Ville**
6 rue du clos simon
Rennes - 35000
FRANCE
<br>

**Artistes du collectif :**
Corentin Le Bris
Arthur Masson
Quentin Orhant
Charly Gutierrez
<br>

<a href="mailto:idlv.contact@gmail.com"><img src="/content/images/2017/05/mail-1_large.png"></a>idlv.contact@gmail.com<br>

<a href="http://twitter.com/collectif_idlv" target="newtab"><img src="/content/images/2017/05/Sans-titre---3-02-1_large.png"></a>twitter.com/collectif_idlv<br>

<a href=" https://www.facebook.com/idlv.co/" target="newtab"><img src="/content/images/2017/05/Sans-titre---3-03_large.png"></a>facebook.com/idlv.co/<br>

